listAllFunctionalities['render_and_view.js'] = function (context, CurrentWorkingMode, definition, id = 'render_and_view.js') { 
  // Start of editable area
    // ------------- define a button and add to top --------------------
    definition[id].RenderModesDropDown = new DropDown({
        'id':"RenderModesDropDown",
        'value': "edges",
        'listOptions': [{label:"GiDSimulation007", value:"edges"},{label:"GiDSimulation008", value:"surfaces_with_edges"},{label:"GiDSimulation009", value:"surfaces"}],
        'name': "GiDSimulation010",
        'tooltip': "GiDSimulation010",
        'onChange': function (value) {
            const renderOptions = ['edges','surfaces_with_edges','surfaces'];
            if (value === renderOptions[0]) {
                definition['element_widget3d.js'].Widget3dGraphicalObject.renderEdges();
            }
            else if (value === renderOptions[1]) {
                definition['element_widget3d.js'].Widget3dGraphicalObject.renderSurfacesWithEdges();
            }
            else if (value === renderOptions[2]) {
                definition['element_widget3d.js'].Widget3dGraphicalObject.renderSurfaces();
            }
            else {
                //value === null
                // No has seleccionado ninguna opción.      
                //console.log("something went wrong, clearValues = false and you get value = null");
            }
        },
        'onInit': function (){
            definition['element_widget3d.js'].Widget3dGraphicalObject.renderEdges();
        }
    });
    definition[id].RefreshView = new Button({
        'id':"RefreshView",
        'tooltip': "GiDSimulation011",
        'icon_type': 'fas',
        'icon': 'sync-alt',
        'onClick': function () {
        definition['element_widget3d.js'].Widget3dGraphicalObject.refreshView();
        },
    });

    definition[id].ViewToolbar = new Toolbar({
        'id':'ViewToolbar',
        'name': 'View Toolbar',
        'children': [
        definition[id].RenderModesDropDown,
        definition[id].RefreshView
        ]
    });
    CurrentWorkingMode.addTop(definition[id].ViewToolbar);
  // End of editable area
}
Dependencies = [ 'globalTranslations.js', 'element_widget3d.js' ];
