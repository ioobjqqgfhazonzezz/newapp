listAllFunctionalities['SeaFEM.js'] = function (context, CurrentWorkingMode, definition, id = 'SeaFEM.js') { 
  // Start of editable area
        
  definition[id].TreeTableF = new TreeTable({
    'name': "Data tree",
    'configuration': "data_tree",  
    'translation': [ { id:"Data tree", translations:{es:"Árbol de datos",en:"Data tree"} }],                 
    'children': [ 
        new container({                    
            'name': "Simulation data",            
            'tooltip': "Define simulation data",
            'icon': 'pi pi-sliders-h',
            'children': [  
                new container({                    
                    'name': "Units",                  
                    'tooltip': "Define units",
                    'icon': 'pi pi-sliders-h',
                    'children': [  
                        new DropDown({                            
                            'listOptions': [{label:"m", value:"m"}, {label:"cm", value:"cm"}, {label:"mm", value: "mm"}],
                            'name': "Geometry units",                                         
                            'value': "m",  
                            'icon': 'pi pi-sliders-h',                                                     
                            'onChange': function (value) {   
                            }
                        }),
                        new DropDown({                            
                            'listOptions': [{label:"Int. system (SI)", value: "Int. system (SI)"}, {label:"Imperial system",value: "Imperial system"}],
                            'name': "Units system",                                        
                            'value': "Int. system (SI)",  
                            'icon': 'pi pi-sliders-h',              
                            'tooltip': "Choose units system",              
                            'onChange': function (value) {   
                            }
                        }), 
                    ]
                }), 
                new container({                    
                    'name': "Gravity",                   
                    'tooltip': "Define gravity",
                    'icon': 'pi pi-sliders-h',
                    'children': [ 
                        new value({
                            'name': "M",                                                 
                            'tooltip': "Define gravity",
                            'icon': "pi pi-file", 
                            'value': "9.80665",
                            'unit': "m/s^2",
                            'unit_magnitude': "L/T^2",
                            'onChange': function (value) {   

                            }                    
                        }),
                    ]
                 }),                        
            ]
        }), 
        new container({                    
            'name': "General data",           
            'tooltip': "Define general data",
            'icon': 'pi pi-folder',
            'children': [ 
                new container({                    
                    'name': "water_density",                   
                    'tooltip': "water_density",
                    'translation':[{ id:"water_density", translations:{es:"Densidad del agua", en: "Water density"} }],
                    'icon': 'pi pi-cog',
                    'children': [  
                        new value({
                            'name': "water_density",                           
                            'translation':[{ id:"water_density", translations:{es:"Densidad del agua", en: "Water density"} }],
                            'tooltip': "Define water density",
                            'icon': "pi pi-cog", 
                            'value': "1025",
                            'unit': "kg/m^3",
                            'unit_magnitude': "M/L^3",
                            'onChange': function (value) {   

                            }                    
                        }),   
                    ]                     
                }),
                new container({                    
                    'name': "Problem setup",                   
                    'tooltip': "Problem setup",
                    'icon': 'pi pi-cog',
                    'children': [ 
                        new container({                    
                            'name': "Analysis type",                            
                            'tooltip': "Define analysis type",
                            'icon': 'pi pi-cog',
                            'children': [ 
                                new value({
                                    'name': "Frequency domain",                                                             
                                    'tooltip' : "Activate frequency domain",
                                    'icon': "pi pi-wallet",
                                    'value': "0",             
                                    'onChange': function (value) {   

                                    }
                                }), 
                                new container({                    
                                    'name': "Time domain type",                                    
                                    'tooltip': "Define time domain type",
                                    'icon': 'pi pi-cog',
                                    'children': [ 
                                        new value({
                                            'name': "1st order diffraction radiation",                                                                  
                                            'tooltip' : "Activate 1st order diffraction radiation",
                                            'icon': "pi pi-wallet",
                                            'value': "1",             
                                            'onChange': function (value) {   

                                            }
                                        }),
                                        new value({
                                            'name': "2nd order diffraction radiation",                                                                     
                                            'tooltip' : "Activate 2nd order diffraction radiation",
                                            'icon': "pi pi-wallet",
                                            'value': "0",             
                                            'onChange': function (value) {   

                                            }       
                                        }),
                                        new value({
                                            'name': "Fatigue damage assessment",                                                                      
                                            'tooltip' : "Activate fatigue damage assessment",
                                            'icon': "pi pi-wallet",
                                            'value': "0",             
                                            'onChange': function (value) {   

                                            }               
                                        }),
                                    ]
                                }),
                            ]
                        }),
                        new container({                    
                            'name': "Environment",                           
                            'tooltip': "Define environment",
                            'icon': 'pi pi-cog',
                            'children': [ 
                                new value({
                                    'name': "Waves",                                                               
                                    'tooltip' : "Activate waves",
                                    'icon': "pi pi-wallet",
                                    'value': "0",             
                                    'onChange': function (value) {   

                                    }   
                                }),
                                new value({
                                    'name': "Current",                                                              
                                    'tooltip' : "Activate current",
                                    'icon': "pi pi-wallet",
                                    'value': "0",             
                                    'onChange': function (value) {   

                                    }
                                }),
                            ]
                        }),
                        new container({                    
                            'name': "Type of analysis",                         
                            'tooltip': "Define type of analysis",
                            'icon': 'pi pi-cog',
                            'children': [ 
                                new value({
                                    'name': "Seakeeping",                                                            
                                    'tooltip' : "Activate seakeeping",
                                    'icon': "pi pi-wallet",
                                    'value': "1",             
                                    'onChange': function (value) {   

                                    }
                                }),
                                new value({
                                    'name': "Turning circle",                                                             
                                    'tooltip' : "Activate turning circle",
                                    'icon': "pi pi-wallet",
                                    'value': "0",             
                                    'onChange': function (value) {   

                                    }
                                }),  
                                new value({
                                    'name': "Towing",                                                             
                                    'tooltip' : "Activate towing",
                                    'icon': "pi pi-wallet",
                                    'value': "0",             
                                    'onChange': function (value) {   

                                    }
                                }),
                                new value({
                                    'name': "Hull girder analysis",                                                              
                                    'tooltip' : "Activate hull girder analysis",
                                    'icon': "pi pi-wallet",
                                    'value': "0",             
                                    'onChange': function (value) {   

                                    }
                                })  
                            ]
                        }),
                    ]
                }),
            ]
        }),        
    ]
})
CurrentWorkingMode.addLeft(definition[id].TreeTableF);
// console.log('treetable',JSON.stringify(definition[id].TreeTableF, null, 4))
// End of editable area
}
