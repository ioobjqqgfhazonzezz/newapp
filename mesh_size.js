listAllFunctionalities['mesh_size.js'] = function (context, CurrentWorkingMode, definition, id = 'mesh_size.js') { 
  // Start of editable area

   definition[id].MeshSize = new Input({
        'id':"MeshSize",
        'name': "GiDSimulation013",
        'tooltip': "GiDSimulation013",
        'value': '-1.0',
    });
    definition['main_top_toolbar.js'].MainToolbar.addButton(definition[id].MeshSize);

  // End of editable area
}
Dependencies = [ 'globalTranslations.js', 'main_top_toolbar.js' ];