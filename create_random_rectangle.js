listAllFunctionalities['create_random_rectangle.js'] = function (context, CurrentWorkingMode, definition, id = 'create_random_rectangle.js') { 
  // Start of editable area

  // ------------- define a button and add to top --------------------
  definition[id].RectangleButton = new Button({
    'tooltip': "btnAddRectangle",
    'icon_type': 'far',
    'icon': 'square',
    'translation': [ { id:"btnAddRectangle", translations:{es:"Añadir rectángulo",en:"Add rectangle"} }],
    'onClick': function () {
      const ll_x = Math.random() * 10.0;
      const ll_y = Math.random() * 10.0;
      const ur_x = Math.random() * 10.0;
      const ur_y = Math.random() * 10.0;
      const z = Math.random() * 10.0;
      context.createRectangle([ll_x, ll_y], [ur_x, ur_y], z);
    },
  });
  definition['main_top_toolbar.js'].MainToolbar.addButton(definition[id].RectangleButton);

  // End of editable area
}
Dependencies = [ 'main_top_toolbar.js' ];