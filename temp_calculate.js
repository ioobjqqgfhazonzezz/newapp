listAllFunctionalities['temp_calculate.js'] = function (context, CurrentWorkingMode, definition, id = 'temp_calculate.js') { 
  // Start of editable area
    definition[id].calcbut = new Button({
        'name': "calcbut",    
        'icon': 'cog',
        'icon_type': 'fas',   
        'translation': [ { id:"calcbut", translations:{es:"Calcular",en:"Calculate"} }],
        'onClick': async function () {  
            console.log('Run calculation...');                 
            let data_tree = definition['tree_table.js'].TreeTableF;          
            var write_calc_data = [];

            write_calc_data.push({
                "WriteString": "=================================================================="
            });
            write_calc_data.push({
                "WriteString": "                        General Data File"
            });
            write_calc_data.push({
                "WriteString": "=================================================================="   
            });
            write_calc_data.push({
                "WriteString": "Units:"
            });  

            let Length = data_tree.GetValue('length');  
            let Mass = data_tree.GetValue('mass'); 
           
            write_calc_data.push({
                "WriteString": 'length' + " " + Length + " " + 'mass' + " " + Mass        
            });  

            write_calc_data.push({
                "WriteString": "Number of elements and nodes:"
            });

            const NumElements = await context.sendGetNumberElements();  
            const NumNodes = await context.sendGetNumberNodes();    

            write_calc_data.push({
                "WriteString": NumElements + " " + NumNodes + "\n"        
            });

            write_calc_data.push({
                "WriteString": ".................................................................\n"
            });

            // #################### COORDINATES ##########################
            write_calc_data.push({
                "WriteString": "Coordinates:"
            });

            let Header_coord = ' '.repeat(2) + "Node" + ' '.repeat(8) + "X" + " " + Length;
            Header_coord += ' '.repeat(15) +  "Y" + " " + Length;
            write_calc_data.push({
                "WriteString": Header_coord        
            });
            write_calc_data.push({
                "WriteCoordinates": {                
                    "factor": "1.0", 
                    "format": "%5d %14.5e %14.5e%.0s\n"              
                }
            }); 

            // #################### CONNECTIVITIES #######################
            write_calc_data.push({
                "WriteString": ".................................................................\n"
            });
            write_calc_data.push({
                "WriteString": "Connectivities:"
            });
            write_calc_data.push({
                "WriteString": "    Element    Node(1)   Node(2)   Node(3)     Material"
            });
                                                        
            let meshtype = context.GetSavedValues('mesh_type.js','MeshTypeDropDown');   
            if (meshtype == "none") { 
                meshtype = "Triangle";
            }                        
            console.log('meshtype',meshtype);

            let ConnectivitiesDict = data_tree.GetConnectivitiesDict('shells');                  
            write_calc_data.push({
                "WriteConnectivities": {
                    "dict": ConnectivitiesDict,
                    "meshtype": meshtype,
                }
            });               

            // #################### MATERIALS ############################
            write_calc_data.push({
                "WriteString": ".................................................................\n" 
            });

            write_calc_data.push({
                "WriteString": "Materials:"
            });
            
            let NumMat = data_tree.GetNumMaterials('shells');        
            write_calc_data.push({
                "WriteString": NumMat 
            });
                            
            write_calc_data.push({
                "WriteString": "Material      Surface density kg/m^2"                 
            });   
            let Materials = data_tree.GetMaterials('shells');             
            let icount = 1;                  
            Materials.forEach(mat => {                              
                let Density = data_tree.GetDensityOfMaterial(mat);
                Density = ' '.repeat(3) + icount + ' '.repeat(2) + Density;
                write_calc_data.push({
                    "WriteString": Density
                }); 
                ++icount;    
            });
            write_calc_data.push({
                "WriteString": "\n"        
            });

            // #################### CONCENTRATE WEIGHTS ##################
            write_calc_data.push({
                "WriteString": ".................................................................\n"
            });
            write_calc_data.push({
                "WriteString": "Concentrate Weights:"
            });

            let Dict = data_tree.GetDict('point_weight');                        
            write_calc_data.push({
                  "NumberNodes": {
                    "dict": Dict,
                }
            });

            write_calc_data.push({
                 "WriteString": "Node   Mass" + " "  + Mass
            }); 
                              
            write_calc_data.push({
                "WriteNodes": {
                    "dict": Dict,
                    "format": "%13.5E",
                }
            });
           
            let mesh_size = context.GetSavedValues('mesh_size.js','MeshSize');                                              
            context.meshModel(mesh_size);            
            context.Write_Calc_Data(write_calc_data);          
            context.runCalculation();
        },
    });
    definition['main_top_toolbar.js'].MainToolbar.addButton(definition[id].calcbut); 
  // End of editable area
}
Dependencies = [ 'main_top_toolbar.js', 'tree_table.js' ];