listAllFunctionalities['data_as_neutral_treetable.js'] = function (context, CurrentWorkingMode, definition, id = 'data_as_neutral_treetable.js') { 
  // Start of editable area
  definition[id].databut = new Button({
    'id':'databut',
    'name': "databut",    
    'icon': 'box', 
    'icon_type': 'fas',   
    'translation': [ { id:"databut", translations:{es:"NuevoData",en:"NewData"} }],
    'onClick': function () {
      context.toggleWindow(definition[id].DataTree, CurrentWorkingMode.mainwindow);
    },
  });
  definition['main_top_toolbar.js'].MainToolbar.addButton(definition[id].databut);
  
  let units_data = new AppUnitsPreferences();      
  var units_system = "SI"; 

  
  definition[id].DataTree = new DataTree({
    'name': "Data tree",
    'translation': [{ id: "Data tree", translations: { es: "Árbol de datos", en: "Data tree" } }],
    'data': [
        new ContainerRow({
            'name': 'Units',
            'tooltip': "Define units",
            'id': 'units',            
            'icon': 'pi pi-sliders-h',
            'translation': [{ id: "Units", translations: { es: "Unidades", en: "Units" } }, { id: "Define units", translations: { es: "Definir unidades", en: "Define units" } }],
            'arrangedata': 'vertical',
            'data': [
                new DropDown({
                    'listOptions': [{ label: '_SI', value: 'SI' }, { label: '_Imperial', value: 'Imperial' }],
                    'name': 'Units system',
                    'id': 'units_system',
                    'value': 'SI',
                    'icon': 'pi pi-sliders-h',
                    'tooltip': "units_system_tooltip",
                    'UIHintStyle': "[]",
                    'translation': [{ id: "Units system", translations: { es: "Sistema de unidades", en: "Units system" } }, { id: "units_system_tooltip", translations: { es: "Definir sistema de unidades", en: "Define units system" } }],
                }),
                new DropDown({
                    'listOptions': units_data.getunit_magnitude("L", units_system),
                    'name': 'Units mesh',
                    'id': 'units_mesh',
                    'value': 'm',
                    'icon': 'pi pi-sliders-h',
                    'tooltip': "units_mesh_tooltip",
                    'translation': [{ id: "Units mesh", translations: { es: "Unidades de geometría", en: "Geometry units" } }, { id: "units_mesh_tooltip", translations: { es: "Definir unidades de geometría", en: "Define geometry units" } }],
                    'UIHintStyle': "[]",
                })
              ],
              'children': [
                new ContainerRow({
                    'name': 'Basic units',
                    'id': 'basic_units',
                    'icon': 'pi pi-bars',
                    'translation': [{ id: "Basic units", translations: { es: "Unidades básicas", en: "Basic units" } }, { id: "basic_units_tooltip", translations: { es: "Unidades básicas a utilizar en el análisis", en: "Basic units to be used in the analysis" } }],
                    'tooltip': "basic_units_tooltip",
                    'arrangedata': 'vertical',
                    'data': [
                        new DropDown({
                            'name': 'Length',
                            'id': 'length',
                            'value': 'm',
                            'listOptions': units_data.getunit_magnitude("L", units_system),
                            'translation': [{ id: "Length", translations: { es: "Longitud", en: "Length" } }, { id: "length_tooltip", translations: { es: "Definir longitud", en: "Define length" } }],
                            'icon': 'pi pi-bars',
                            'tooltip': "length_tooltip"
                        }),
                        new DropDown({
                            'name': 'Mass',
                            'id': 'mass',
                            'value': 'kg',
                            'listOptions': units_data.getunit_magnitude("M", units_system),
                            'icon': 'pi pi-bars',
                            'translation': [{ id: "Mass", translations: { es: "Masa", en: "Mass" } },
                            { id: "mass_tooltip", translations: { es: "Definir masa", en: "Define mass" } }],
                            'tooltip': "mass_tooltip"
                        })
                    ]
                })
            ]
        }),
        new ConditionRow({            
            'name': 'Point weight',
            'id': 'point_weight',
            'tooltip': "point_weight_tooltip",
            'icon': 'pi pi-filter',
            'UIHintStyle': "[]",
            'translation': [{ id: "Point weight", translations: { es: "Peso local", en: "Point weight" } },
            { id: "point_weight_tooltip", translations: { es: "Definir peso local", en: "Define point weight" } }],
            'over': 'point',
            'data': [
                new Input({
                    'name': 'Weight',
                    'id': 'weight',
                    'tooltip': "weight_tooltip",
                    'icon': 'pi pi-filter',
                    'value': '0.0',
                    'unit': 'kg',
                    'translation': [{ id: "Weight", translations: { es: "Peso", en: "Weight" } },
                    { id: "weight_tooltip", translations: { es: "Especifique el peso que desea aplicar", en: "Specify the weight that you want to apply" } }],
                    'unit_magnitude': 'M',
                }),
                new value({
                    'name': 'Density',
                    'id': 'desity',
                    'tooltip': "weight_tooltip",
                    'icon': 'pi pi-filter',
                    'value': '0.0',
                    'unit': 'kg/m',
                    'translation': [{ id: "Weight", translations: { es: "Peso", en: "Weight" } },
                    { id: "weight_tooltip", translations: { es: "Especifique el peso que desea aplicar", en: "Specify the weight that you want to apply" } }],
                    'unit_magnitude': 'M',
                }),
            ]
        }),
        new ContainerRow({
            'name': 'Properties',
            'id': 'properties',
            'tooltip': "properties_tooltip",
            'icon': 'pi pi-inbox',
            'UIHintStyle': "[]",
            'translation': [{ id: "Properties", translations: { es: "Propiedades", en: "Properties" } },
            { id: "properties_tooltip", translations: { es: "Definir propiedades y materiales", en: "Define properties and materials" } }],
            'contextualmenu':['ViewThis','Separator1','Toggle','ExpandAll','CollapseAll'],
            'children': [
                new ConditionRow({                    
                    'name': 'Shells',
                    // 'id': 'shells',
                    'tooltip': "shells_tooltip",
                    "icon": 'pi pi-inbox',
                    'UIHintStyle': "[]",
                    'over': 'surface',
                    'translation': [{ id: "Shells", translations: { es: "Planchas", en: "Shells" } },
                    { id: "shells_tooltip", translations: { es: "Seleccione su material y la superficie relacionada con este", en: "Select your material and the surfaces related to it" } }],
                    'data': [
                        new DropDown({
                            'listOptions': [{ label: 'Air', value: 'Air' },
                            { label: 'Steel', value: 'Steel' },
                            { label: 'Aluminium', value: 'Aluminium' }],
                            'name': 'Material',
                            // 'id': 'material',
                            'value': "Air",
                            'icon': 'pi pi-bars',
                            'tooltip': "material_tooltip",
                            'UIHintStyle': "[]",
                            'translation': [{ id: "Material", translations: { es: "Material", en: "Material" } },
                            { id: "material_tooltip", translations: { es: "Seleccione un material de la base de datos", en: "Choose a material from materials database" } }],
                        })
                    ]
                }),
                new BlockDataGeneratorRow({
                    'name': 'Materials',
                    'id': 'materials',
                    'tooltip': "materials_tooltip",
                    'icon': 'pi pi-file',
                    'UIHintStyle': "[]",
                    'translation': [{ id: "Materials", translations: { es: "Materiales", en: "Materials" } },
                    { id: "materials_tooltip", translations: { es: "Base de datos de materiales", en: "Materials database" } }],
                    'contextualmenu':['ViewThis','Separator1','NewBlockData','Separator2','Toggle','ExpandAll','CollapseAll'],
                    'template': function() {
                        return new ContainerRow({
                            'name': 'Air',
                            'editablename': 'true',                            
                            'tooltip': "material_prop",
                            'icon': 'pi pi-file',
                            'UIHintStyle': "[]",
                            'translation': [{ id: "material_prop", translations: { es: "Propiedades del material", en: "Material properties" } }],
                            'contextualmenu':['ViewThis','Separator1','CopyBlockData','DeleteBlockData','Separator2','Toggle','ExpandAll','CollapseAll'],
                            'data': [
                                new Input({
                                    'name': 'Density',                                    
                                    'tooltip': "density_tooltip",
                                    'icon': 'pi pi-file',
                                    'value': '1.01',
                                    'unit': 'kg/m^2',
                                    'unit_magnitude': 'M/L^2',
                                    'UIHintStyle': "[]",
                                    'translation': [{ id: "Density", translations: { es: "Densidad", en: "Density" } },
                                    { id: "density_tooltip", translations: { es: "Densidad superficial asumiendo un espesor de 1 metro", en: "Superficial density assuming a thickness of 1 meter" } }],
                                })
                            ]
                        })
                    },
                })
             ]
        })
    ]    
});
  
  CurrentWorkingMode.addInSplitRight(definition[id].DataTree);  
  // End of editable area
}
Dependencies = [ 'main_top_toolbar.js', 'globalTranslations.js' ];