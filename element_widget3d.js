listAllFunctionalities['element_widget3d.js'] = function (context, CurrentWorkingMode, definition, id = 'element_widget3d.js') { 
  // Start of editable area
    definition[id].Widget3dGraphicalObject = new GraphicalObject({
        'id':'Widget3dGraphicalObject',
        'name': 'webgl',
        'onNewSelectedObject': function () { },
    });
    // ------------- definition of functions associated to events --------------------
    context.onNewPoint(async function newPoint(gididpoint) {
        let result = await context.getPointInfo(gididpoint);
        definition[id].Widget3dGraphicalObject.addDrawingObject(Object.assign({ type: 'point', id: gididpoint }, result));
    });
    context.onNewLine(async function newLine(gididline) {
        let result = await context.getLineInfo(gididline);
        definition[id].Widget3dGraphicalObject.addDrawingObject(Object.assign({ type: 'line', id: gididline }, result));
    });
    context.onNewSurface(async function newSurface(gididsurface) {
        console.log('onnewsurface');
        let result = await context.getSurfaceInfo(gididsurface);
        definition[id].Widget3dGraphicalObject.addDrawingObject(Object.assign({ type: 'surface', id: gididsurface }, result));
    });
    // ------------- end: definition of functions associated to events --------------------
    CurrentWorkingMode.addMiddle(definition[id].Widget3dGraphicalObject);
  // End of editable area
}
