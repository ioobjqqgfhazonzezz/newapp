listAllFunctionalities['seafem_treetable.js'] = function (context, CurrentWorkingMode, definition, id = 'seafem_treetable.js') { 
  // Start of editable area
  definition[id].databut = new Button({
    'id':'databut',
    'name': "databut",    
    'icon': 'box', 
    'icon_type': 'fas',   
    'translation': [ { id:"databut", translations:{es:"NuevoData",en:"NewData"} }],
    'onClick': function () {
      context.toggleWindow(definition[id].DataTree, CurrentWorkingMode.mainwindow);
    },
  });
  definition['main_top_toolbar.js'].MainToolbar.addButton(definition[id].databut);
  
  let units_data = new AppUnitsPreferences();      
  var units_system = "SI"; 
  
  definition[id].DataTree = new DataTree({
    'name': "Data tree",
    'translation': [{ id: "Data tree", translations: { es: "Árbol de datos", en: "Data tree" } }],
    'data': [
        new ContainerRow({
            'name': 'Simulation data',
            'tooltip': "Define simulation data",
            'id': 'simulation_data',            
            'icon': 'pi pi-sliders-h',
            'translation': [{ id: "Simulation data", translations: { es: "Datos de simulación", en: "Simulation data" } }],            
            'data': [                                            
              ],
              'children': [
                new ContainerRow({
                    'name': 'Units',
                    'id': 'units',
                    'icon': 'pi pi-bars',
                    'tooltip': "Units to be used in the analysis",
                    'translation': [{ id: "units", translations: { es: "Unidades", en: "Units" } }],                                        
                    'data': [
                        new DropDown({
                            'name': 'Geometry units',
                            'id': 'geom_units',
                            'value': 'm',
                            'tooltip': "Define geometry units",
                            'listOptions': units_data.getunit_magnitude("L", units_system),
                            'translation': [{ id: "Geometry units", translations: { es: "Unidades de geometría", en: "Geometry units" } }],                             
                            'icon': 'pi pi-bars',                           
                        }),
                        new DropDown({
                            'listOptions': [{ label: '_SI', value: 'SI' }, { label: '_Imperial', value: 'Imperial' }],
                            'name': 'Units system',
                            'id': 'units_system',
                            'value': 'SI',
                            'icon': 'pi pi-sliders-h',
                            'tooltip': "Define units system",
                            'UIHintStyle': "[]",
                            'translation': [{ id: "Units system", translations: { es: "Sistema de unidades", en: "Units system" } }],
                        }),                                          
                    ]
                }),
                new ContainerRow({
                    'name': 'Gravity',
                    'id': 'gravity',
                    'icon': 'pi pi-bars',
                    'tooltip': "Define gravity",
                    'translation': [{ id: "gravity", translations: { es: "Gravedad", en: "Gravity" } }],                                         
                    'data': [ 
                        new Input({
                            'name': 'Magnitude',                                    
                            'tooltip': "Magnitude of the acceleration of the gravity",
                            'icon': 'pi pi-file',
                            'value': '9.80665',
                            'unit': 'kg/m^2',
                            'unit_magnitude': 'M/L^2',
                            'UIHintStyle': "[]",
                            'translation': [{ id: "Magnitude", translations: { es: "Aceleración de la gravedad", en: "Acceleration of the gravity" } }],
                        })                                                            
                    ]
                })
            ]
        }),
        new ConditionRow({            
            'name': 'Point weight',
            'id': 'point_weight',
            'tooltip': "Define point weight",
            'icon': 'pi pi-filter',
            'UIHintStyle': "[]",
            'translation': [{ id: "Point weight", translations: { es: "Peso local", en: "Point weight" } }],
            'over': 'point',
            'data': [
                new Input({
                    'name': 'Weight',
                    'id': 'weight',
                    'tooltip': "Specify the weight that you want to apply",
                    'icon': 'pi pi-filter',
                    'value': '0.0',
                    'unit': 'kg',
                    'translation': [{ id: "Weight", translations: { es: "Peso", en: "Weight" } }],
                    'unit_magnitude': 'M',
                }),
                new value({
                    'name': 'Density',
                    'id': 'desity',
                    'tooltip': "Define density",
                    'icon': 'pi pi-filter',
                    'value': '0.0',
                    'unit': 'kg/m',
                    'translation': [{ id: "Density", translations: { es: "Densidad", en: "Density" } }],
                    'unit_magnitude': 'M',
                }),
            ]
        }),     
        new ContainerRow({
            'name': 'Properties',
            'id': 'properties',
            'tooltip': "properties_tooltip",
            'icon': 'pi pi-inbox',
            'UIHintStyle': "[]",
            'translation': [{ id: "Properties", translations: { es: "Propiedades", en: "Properties" } },
            { id: "properties_tooltip", translations: { es: "Definir propiedades y materiales", en: "Define properties and materials" } }],
            'contextualmenu':['ViewThis','Separator1','Toggle','ExpandAll','CollapseAll'],
            'children': [
                new ConditionRow({                    
                    'name': 'Shells',
                    // 'id': 'shells',
                    'tooltip': "Select your material and the surfaces related to it",
                    "icon": 'pi pi-inbox',
                    'UIHintStyle': "[]",
                    'over': 'surface',
                    'translation': [{ id: "Shells", translations: { es: "Planchas", en: "Shells" } }],
                    'data': [
                        new DropDown({
                            'listOptions': [{ label: 'Air', value: 'Air' },
                            { label: 'Steel', value: 'Steel' },
                            { label: 'Aluminium', value: 'Aluminium' }],
                            'name': 'Material',
                            // 'id': 'material',
                            'value': "Air",
                            'icon': 'pi pi-bars',
                            'tooltip': "Choose a material from materials database",
                            'UIHintStyle': "[]",
                            'translation': [{ id: "Material", translations: { es: "Material", en: "Material" } }],
                        })
                    ]
                }),
                new BlockDataGeneratorRow({
                    'name': 'Materials',
                    'id': 'materials',
                    'tooltip': "Materials database",
                    'icon': 'pi pi-file',
                    'UIHintStyle': "[]",
                    'translation': [{ id: "Materials", translations: { es: "Materiales", en: "Materials" } }],                    
                    'contextualmenu':['ViewThis','Separator1','NewBlockData','Separator2','Toggle','ExpandAll','CollapseAll'],
                    'template': function() {
                        return new ContainerRow({
                            'name': 'Air',
                            'editablename': 'true',                            
                            'tooltip': "Material properties",
                            'icon': 'pi pi-file',
                            'UIHintStyle': "[]",
                            'contextualmenu':['ViewThis','Separator1','CopyBlockData','DeleteBlockData','Separator2','Toggle','ExpandAll','CollapseAll'],
                            'data': [
                                new Input({
                                    'name': 'Density',                                    
                                    'tooltip': "Superficial density assuming a thickness of 1 meter",
                                    'icon': 'pi pi-file',
                                    'value': '1.01',
                                    'unit': 'kg/m^2',
                                    'unit_magnitude': 'M/L^2',
                                    'UIHintStyle': "[]",
                                    'translation': [{ id: "Density", translations: { es: "Densidad", en: "Density" } }],
                                })
                            ]
                        })
                    },
                })
             ]
        }),
        new ContainerRow({
            'name': 'General data',
            'tooltip': "Define general data",
            'id': 'general_data',            
            'icon': 'pi pi-folder',
            'translation': [{ id: "General data", translations: { es: "Datos generales", en: "General data" } }],            
            'data': [                                            
              ],
              'children': [
                new ContainerRow({
                    'name': 'Water density',
                    'id': 'water_density',
                    'icon': 'pi pi-cog',
                    'tooltip': "Define water density",
                    'translation': [{ id: "Water density", translations: { es: "Densidad del agua", en: "Water density" } }],                                        
                    'data': [
                        new Input({
                            'name': 'Water density',                                    
                            'tooltip': "Water density value",
                            'icon': 'pi pi-cog',
                            'value': '1025',
                            'unit': 'kg/m^3',
                            'unit_magnitude': 'M/L^3',
                            'UIHintStyle': "[]",
                            'translation': [{ id: "Water density", translations: { es: "Densidad del agua", en: "Water density" } }],
                        })                                                                
                    ]
                }),
                new ContainerRow({
                    'name': 'Problem setup',
                    'id': 'problem_setup',
                    'icon': 'pi pi-cog',
                    'tooltip': "Define problem",
                    'translation': [{ id: "Problem setup", translations: { es: "Configurar problema", en: "Problem setup" } }],                                         
                    'children': [                    
                        new ContainerRow({
                            'name': 'Analysis type',
                            'id': 'analysis_type',
                            'icon': 'pi pi-cog',
                            'tooltip': "Define analysis type",
                            'translation': [{ id: "analysis_type", translations: { es: "Configurar problema", en: "Problem setup" } }],                                         
                            'data': [ 
                                new Input({
                                    'name': 'Frequency domain',                                    
                                    'tooltip': "Frequency domain",
                                    'icon': 'pi pi-wallet',
                                    'value': '0',
                                    'UIHintStyle': "[]",
                                    'translation': [{ id: "Frequency domain", translations: { es: "Dominio de la frecuencia", en: "Frequency domain" } }],
                                })                                                         
                            ],
                            'children': [
                                new ContainerRow({
                                    'name': 'Time domain type',
                                    'id': 'time_domain_type',
                                    'icon': 'pi pi-cog',
                                    'tooltip': "Define time domain type",
                                    'translation': [{ id: "Time domain type", translations: { es: "Time domain type", en: "Time domain type" } }],                                         
                                    'data': [ 
                                        new Input({
                                            'name': '1st order diffraction radiation',                                    
                                            'tooltip': "Activate 1st order diffraction radiation",
                                            'icon': 'pi pi-wallet',
                                            'value': '1',
                                            'UIHintStyle': "[]",
                                            'translation': [{ id: "1st order diffraction radiation", translations: { es: "1st order diffraction radiation", en: "1st order diffraction radiation" } }],
                                        }),
                                         new Input({
                                            'name': '2nd order diffraction radiation',                                    
                                            'tooltip': "Activate 2nd order diffraction radiation",
                                            'icon': 'pi pi-wallet',
                                            'value': '0',
                                            'UIHintStyle': "[]",
                                            'translation': [{ id: "2nd order diffraction radiation", translations: { es: "2nd order diffraction radiation", en: "" } }],
                                        }),
                                        new Input({
                                            'name': 'Fatigue damage assessment',                                    
                                            'tooltip': "Activate fatigue damage assessment",
                                            'icon': 'pi pi-wallet',
                                            'value': '0',
                                            'UIHintStyle': "[]",
                                            'translation': [{ id: "Fatigue damage assessment", translations: { es: "Fatigue damage assessment", en: "Fatigue damage assessment" } }],
                                        }),                                                         
                                    ],
                                })
                            ]                            
                        }),
                         new ContainerRow({
                            'name': 'Environment',
                            'id': 'environment',
                            'icon': 'pi pi-cog',
                            'tooltip': "Define environment data",
                            'translation': [{ id: "Environment", translations: { es: "Environment", en: "Environment" } }],                                         
                            'data': [ 
                                new Input({
                                    'name': 'Waves',                                    
                                    'tooltip': "Waves",
                                    'icon': 'pi pi-wallet',
                                    'value': '0',
                                    'UIHintStyle': "[]",
                                    'translation': [{ id: "Waves", translations: { es: "Olas", en: "Waves" } }],
                                }), 
                                new Input({
                                    'name': 'Current',                                    
                                    'tooltip': "Current",
                                    'icon': 'pi pi-wallet',
                                    'value': '0',
                                    'UIHintStyle': "[]",
                                    'translation': [{ id: "Current", translations: { es: "Current", en: "Current" } }],
                                }),                                                                                     
                            ]
                        }),
                         new ContainerRow({
                            'name': 'Type of analysis',
                            'id': 'type_of_analysis',
                            'icon': 'pi pi-cog',
                            'tooltip': "Define type of analysis",
                            'translation': [{ id: "Type of analysis", translations: { es: "Tipo de análisis", en: "Analysis type" } }],                                         
                            'data': [ 
                                new Input({
                                    'name': "Seakeeping",                                                            
                                    'tooltip' : "Activate seakeeping",
                                    'icon': "pi pi-wallet",
                                    'value': "1",   
                                    'UIHintStyle': "[]", 
                                    'translation': [{ id: "Seakeeping", translations: { es: "Seakeeping", en: "Seakeeping" } }],         
                                    'onChange': function (value) {   

                                    }
                                }),
                                new Input({
                                    'name': "Turning circle",                                                             
                                    'tooltip' : "Activate turning circle",
                                    'icon': "pi pi-wallet",
                                    'value': "0",  
                                    'UIHintStyle': "[]",   
                                    'translation': [{ id: "Turning circle", translations: { es: "Turning circle", en: "Turning circle" } }],        
                                    'onChange': function (value) {   

                                    }
                                }),  
                                new Input({
                                    'name': "Towing",                                                             
                                    'tooltip' : "Activate towing",
                                    'icon': "pi pi-wallet",
                                    'value': "0",   
                                    'UIHintStyle': "[]",  
                                    'translation': [{ id: "Towing", translations: { es: "Towing", en: "Towing" } }],        
                                    'onChange': function (value) {   

                                    }
                                }),
                                new Input({
                                    'name': "Hull girder analysis",                                                              
                                    'tooltip' : "Activate hull girder analysis",
                                    'icon': "pi pi-wallet",
                                    'value': "0",    
                                    'UIHintStyle': "[]",      
                                    'translation': [{ id: "Hull girder analysis", translations: { es: "Hull girder analysis", en: "Hull girder analysis" } }],   
                                    'onChange': function (value) {   

                                    }
                                })                                                      
                            ]
                        }),                                                          
                    ]
                })
            ]
        }),
    ]        
});
  
  CurrentWorkingMode.addInSplitRight(definition[id].DataTree);  
  // End of editable area
}
Dependencies = [ 'main_top_toolbar.js', 'globalTranslations_SeaFEM.js' ];