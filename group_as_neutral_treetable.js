listAllFunctionalities['group_as_neutral_treetable.js'] = function (context, CurrentWorkingMode, definition, id = 'group_as_neutral_treetable.js') { 
  // Start of editable area
  definition[id].grbut = new Button({
    'id':'grbut',
    'name': "grbut",    
    'icon': 'box',
    'icon_type': 'fas',   
    'translation': [ { id:"grbut", translations:{es:"Nuevos Grupos",en:"New groups"} }],
    'onClick': function () {
      context.toggleWindow(definition[id].GroupsTree, CurrentWorkingMode.mainwindow);
    },
  });
  definition['main_top_toolbar.js'].MainToolbar.addButton(definition[id].grbut);
  
  definition[id].GroupsTree = new GroupsTree({
    'name': "groups", 
    'id': "groups",
    'children': []
  });
  
  CurrentWorkingMode.addInSplitRight(definition[id].GroupsTree);  
  // End of editable area
}
Dependencies = [ 'main_top_toolbar.js' ];