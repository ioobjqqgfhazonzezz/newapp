listAllFunctionalities['auto_mesh.js'] = function (context, CurrentWorkingMode, definition, id = 'auto_mesh.js') { 
  // Start of editable area
    let mesh_size = context.GetSavedValues('mesh_size.js','MeshSize');
    context.meshModel(mesh_size);
  // End of editable area
}
Dependencies = [ 'mesh_size.js' ];