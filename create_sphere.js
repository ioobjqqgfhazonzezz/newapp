listAllFunctionalities['create_sphere.js'] = function (context, CurrentWorkingMode, definition, id = 'create_sphere.js') { 
  // Start of editable area
    // ------------- define a button and add to top --------------------
    definition[id].SphereButton = new Button({
    'id':'SphereButton',
    'tooltip': "btnAddSphere",
    'icon_type': 'fas',
    'icon': 'circle',
    'translation': [ { id:"btnAddSphere", translations:{es:"Añadir esfera",en:"Add sphere"} }],
    'onClick': function () {
      context.openWindow(definition[id].CreateSphereWindow, CurrentWorkingMode.mainwindow);
    },
  });
  definition['main_top_toolbar.js'].MainToolbar.addButton(definition[id].SphereButton)

  // ------------- define the window --------------------
  definition[id].CreateSphereWindow = new Window({
    'id':'CreateSphereWindow',
    'name': "windSphere",
    'acceptLabel': "GiDSimulation005",
    'translation': [ { id:"windSphere", translations:{es:"Crear Esfera",en:"Create Sphere"} }],
    'children': [
        definition[id].SphereCenterCoordinates = new Coordinates({
          'id':'SphereCenterCoordinates',
          'name': 'center',
          'value': ['0', '1', '2'],
        }),
        definition[id].SphereRadiusInput = new Input({
          'id':'SphereRadiusInput',
          'name': "GiDSimulation006",
          'tooltip': "GiDSimulation006",
          'value': '1',
        }),
      ],
    'onAccept': async function () {
      let center = await definition[id].SphereCenterCoordinates.value();
      context.createSphere(center, await definition[id].SphereRadiusInput.value());
    },
  });
  CurrentWorkingMode.addInSplitLeft(definition[id].CreateSphereWindow);
  // End of editable area
}
Dependencies = [ 'globalTranslations.js', 'main_top_toolbar.js' ];