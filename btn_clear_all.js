listAllFunctionalities['btn_clear_all.js'] = function (context, CurrentWorkingMode, definition, id = 'btn_clear_all.js') { 
  // Start of editable area
    
    // ------------- define a button and add to top --------------------
    definition[id].ClearAllButton = new Button({
        'tooltip': "btnClearAll",
        'icon_type': 'far',
        'icon': 'trash-alt',
        'translation': [ { id:"btnClearAll", translations:{es:"Borrar Todo",en:"Clear All"} }],
        'onClick': function () {
        definition['element_widget3d.js'].Widget3dGraphicalObject.clearAll();
        },
    });
    definition['main_top_toolbar.js'].MainToolbar.addButton(definition[id].ClearAllButton);
  // End of editable area
}
Dependencies = [ 'main_top_toolbar.js', 'element_widget3d.js' ];