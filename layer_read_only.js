listAllFunctionalities['layer_read_only.js'] = function (context, CurrentWorkingMode, definition, id = 'layer_read_only.js') { 
  // Start of editable area
  class LayerRow_read extends TableRow {
    constructor(props) {
      let clone = () => { return new LayerRow_read({...props})}
        let properties = {
            'columns': [],
            'children': [],
            'contextualmenu':[]
        }
        super(properties);

        let layer_row_reference = this;                    
        let gidid = props.gidid; 

        this.defineColumns([
            new Label({
                'name': props.name,
                'tooltip': 'Change name'                                                                
            }),
            new ColorPicker({
                'tooltip': 'Change color',
                'color_format': props.color_format,
                'color': props.color,
                'onHide': function (value) {
                    context.setLayerProperty(gidid,"color",value);
                }
            }),            
            new Button({
                'tooltip': 'Visible',
                'UIScssClass': 'p-button-text',
                'icon_type': 'far',
                'icon': props.visible ? 'eye' : 'eye-slash',
                'onClick': function () {
                    let value = 0;
                    if (this.getIcon() == 'eye') {
                        this.changeIcon('eye-slash');
                        layer_row_reference.setChildrenNotVisible();
                    } else {
                        this.changeIcon('eye');
                        layer_row_reference.setChildrenVisible()
                        value = 1;
                    }
                    context.setLayerProperty(gidid,"visibility",value);
                }
            }),            
            new Button({
                'tooltip': 'Transparent or Opaque',
                'UIScssClass': 'p-button-text',
                'icon_type': 'fas',
                'icon': props.transparent ? 'table-cells-large' : 'stop',
                'onClick': function () {
                    let value = 1;
                    if (this.getIcon() == 'table-cells-large') {
                        this.changeIcon('stop');
                    } else {
                        value = 0;
                        this.changeIcon('table-cells-large');
                    }
                    context.setLayerProperty(gidid,"transparency",value);
                }               
            })            
        ]);

        this.defineContextualMenu(props.contextualmenu)
    
        this.gidid = props.gidid;
        this.name = props.name;
        this.treereference = props.treereference;    
        this.clone=clone;   
    }

    findChildren(name) {
      //console.log('tableRow finding: '+name+' in: ',this.children)
      for (let i = 0; i < this.children.length; i++) {
          if (this.children[i].getName() == name) {
              return this.children[i];
          }
        }
    }
    findChildrenRecursive(name, expr) {
        let index = name.indexOf(expr);
        if (index !== -1) {
            let rootname = name.slice(0, index);
            let rootnode = this.findChildren(rootname);
            return rootnode.findChildrenRecursive(name.slice(index + expr.length, name.length), expr);
        } else {
            return this.findChildren(name);
        }
    }
    setLayerToUse() {
        if ((this.getColumn(2)).getIcon() == 'none') {
            (this.getColumn(2)).executeClick();
        }
    }
    isLayerToUse() {
        let islayerinuse = true
        if ((this.getColumn(2)).getIcon() == 'none') {
            islayerinuse = false;
        }
        return islayerinuse;
    }
    setNotLayerToUse() {
        if ((this.getColumn(2)).getIcon() == 'star') {
            (this.getColumn(2)).changeIcon('none');
            //executeClick do not change from star to none, mantain star   
        }
    }
    isFrozen() {
        let isFrozen = true
        if ((this.getColumn(4)).getIcon() == 'lock-open') {
            isFrozen = false;
        }
        return isFrozen;
    }
    setUnFreeze() {
        if ((this.getColumn(4)).getIcon() == 'lock') {
            (this.getColumn(4)).executeClick();
        }
    }
    setFreeze() {
        if ((this.getColumn(4)).getIcon() == 'lock-open') {
            (this.getColumn(4)).executeClick();
        }
    }
    setPreviousLayerToUseToNone() {
        this.treereference.changeLayerToUsePreviousToNone(this)
    }
    getName() {
        return this.name;
    }
    setVisible() {
        if ((this.getColumn(3)).getIcon() == 'eye-slash') {
            (this.getColumn(3)).executeClick();
        }
    }
    setNonVisible() {
        if ((this.getColumn(3)).getIcon() == 'eye') {
            (this.getColumn(3)).executeClick();
        }
    }
    setChildrenVisible() {
        this.children.forEach(child => child.setVisible());
    }
    setChildrenNotVisible() {
        this.children.forEach(child => child.setNonVisible());
    }
    ifIsLayerToUseChangeToAnother() {
        if (this.isLayerToUse()) {
            this.setNotLayerToUse();
            this.treereference.SelectUnfreezedLayerToUse();
        }
    }

}

class LayersTree_read extends ExtendedWidget {
    constructor(properties) {
        super(properties);

        let SendToSplitBtn;     
        this.layersallproperties = [];
        this.layersfromGiD = [];     
        let laytree = this;
        this.widget = new TreeTableNew({
            'name': properties.name,
            'id': properties.id,
            'onInit': async () => {
                let aux = await context.sendGetLayersAllProperties();                
                this.fillLayers(JSON.parse(aux))               
            },
            'header': new Toolbar({
                'name': 'Toolbar',
                'children': [
                    new Button({
                        'id': 'ListLayerEntities',
                        'tooltip': 'GiDSimulation_ListLayerEntities',
                        'UIScssClass': 'p-button-outlined',
                        'icon_type': 'fas',
                        'icon': 'list',
                        'onClick': async function () {                           
                            let aux = await context.sendGetListLayerEntities("Layer0");
                            console.log('List layer entities',aux);
                        },
                    })
                ]
            }),
            'columns': [
                { header: 'layer', width: '70%' },
                { header: 'color', width: '10%' },
                { header: 'visibility', width: '10%' },
                { header: 'transparent', width: '10%' }
            ],
            'contextualmenuoptions':[                
                new MenuItem({
                    'id':'Toggle',
                    'label': 'GiDSimulation_Toggle', 
                    'icon': 'fas fa-sort', 
                    'command':function([id]) {
                        console.log("Press ContextualMenu Toggle");                              
                    }
                }),
                new MenuItem({
                    'id':'ExpandAll',
                    'label': 'GiDSimulation_ExpandAll', 
                    'icon': 'fas fa-angles-down', 
                    'command':function([id]) {
                        console.log("Press ContextualMenu ExpandAll");                                  
                    }
                }),
                new MenuItem({
                    'id':'CollapseAll',
                    'label': 'GiDSimulation_CollapseAll', 
                    'icon': 'fas fa-angles-up', 
                    'command':function([id]) {
                        console.log("Press ContextualMenu CollapseAll");                                 
                    }
                })  
            ],
            'data': []
        });
    }
    findChildren(name) {
        console.log('name',name);
      let rowiterator = this.widget.getFirstRow();
      do {
        if (rowiterator.getName() == name) {
          return rowiterator;
        }
      } while (rowiterator = this.widget.getNextRowWithoutEnterInChildren(rowiterator));
      return null;
    }
    findChildrenRecursive(name, expr) {
        let index = name.indexOf(expr);
        if (index !== -1) {
            let rootname = name.slice(0, index);
            let rootnode = this.findChildren(rootname);
            return rootnode.findChildrenRecursive(name.slice(index + expr.length, name.length), expr);
        } else {
            return this.findChildren(name);
        }
    }
    listLayerEntities() {

    }
    changeLayerToUsePreviousToNone(current_layer_to_use) {
        this.current_layer_to_use.setNotLayerToUse();
        this.current_layer_to_use = current_layer_to_use;
    }
    SelectUnfreezedLayerToUse() {
        let rowiterator = this.widget.getFirstRow();
        do {
            if (!rowiterator.isFrozen()) {
                rowiterator.setLayerToUse();
                return;
            }
        } while (rowiterator = this.widget.getNextRow(rowiterator));
        //totes estan froozen
        rowiterator = this.widget.getFirstRow();
        rowiterator.setUnFreeze();
        rowiterator.setLayerToUse();
    }  
    deletelayer() {   
        let private_id = this.widget.getSelectedRow();                        
        let current_node = idToClassElement[currentWKM][private_id];        
        let gidid = current_node.gidid;
        const index_gr = this.layersfromGiD.indexOf(gidid);
        if (index_gr > -1) {
            this.layersfromGiD.splice(index_gr,1); 
        }         
        let ischild = this.is_child(current_node);       
        if (!ischild) {           
            this.widget.removeChildren(current_node);            
        } else {
            let node_parent = current_node.parent;
            let index = node_parent.children.indexOf(current_node);
            node_parent.children.splice(index, 1);                 
        }      
        context.deleteLayer(gidid);      
        this.widget.update(['data']);
    }
    is_child(node) {
        let expr = '//'; 
        let ischild = 0;
        if (node.gidid.indexOf(expr) !== -1) {
            ischild = 1
        }
        return ischild;
    }
    newchildlayer() {        
        let private_id = this.widget.getSelectedRow();         
        let current_node = idToClassElement[currentWKM][private_id];       
        let gidid = current_node.gidid;    
        let expr = '//';              
        let color = this.random_hex_color();      
        let layername = this.new_child_name(current_node.children,"Layer");
        let newlayername = gidid + expr + layername;          
        let newlayer = new LayerRow_read({
            'treereference': this,
            'gidid': newlayername,
            'name': this.child_name(newlayername),
            'color_format': 'hex', 
            'color': color,          
            'layertouse': 1,
            'visible': 1,
            'freeze': 0,
            'transparent': 0,
            'back': 0
        });          
        let p_name = this.parent_name(newlayername);
        let curr_node = this.findChildrenRecursive(p_name, expr);
        if (curr_node) {
            curr_node.addChildren(newlayer);  
        }   
        context.createLayer(newlayername,color);
        this.widget.update(['data']);             
        this.layersfromGiD.push(newlayername);        
    }
    newlayer() {   
        this.unset_layer_to_use();                   
        let color = this.random_hex_color();      
        let layername = this.new_name("Layer");
        let newlayer = new LayerRow_read({
            'treereference': this,
            'gidid': layername,
            'name': this.child_name(layername),
            'color_format': 'hex', 
            'color': color,          
            'layertouse': 1,
            'visible': 1,
            'freeze': 0,
            'transparent': 0,
            'back': 0
        });                      
        this.widget.addChildren(newlayer);       
        context.createLayer(layername,color);                     
        this.layersfromGiD.push(layername);       
        this.widget.selectedrow = this.private_id;              
        this.widget.update(['data']);                            
    }  
    unset_layer_to_use() {
        this.widget.data.forEach(childNode => {                      
            if ((childNode.getColumn(2)).getIcon() == 'star') {
                (childNode.getColumn(2)).changeIcon('none');                
            }
        });        
    }
    labelList() {
        let labelList = [];               
        this.widget.data.forEach(childNode => {
            labelList.push(childNode.name);
        });       
        return labelList;
    };
    new_child_name (node, laygr) {
        let itList = [];
        for (let i = 0; i < node.length; i++) {
            itList += this.child_name(node[i].name);
        }
        let incr = 0;
        let new_name = laygr + incr;
        while (itList.includes(new_name)) {
            ++incr;
            new_name = laygr + incr;
        }
        console.log('new_name',new_name);
        return new_name;
    }   
    new_name(name) {
        let namesList = this.labelList();
        let icount = 0;
        let new_name = name + icount;
        while (namesList.includes(new_name)) {
            ++icount;
            new_name = name + icount;
        }
        return new_name;
    };
    random_hex_color() {
        var letters = '0123456789ABCDEF';
        let color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }
    child_name (name) {
        let expr = '//';   
        if (name.indexOf(expr) !== -1) {
            let index = name.lastIndexOf(expr);
            name = name.slice(index + expr.length);
        }
        return name
    }
    parent_name (name) {
        let expr = '//';   
        if (name.indexOf(expr) !== -1) {
            let index = name.lastIndexOf(expr);
            name = name.slice(0, index);
        }
        return name
    }
    fillLayers(layersallproperties) {
        let expr = '//';
        this.layersallproperties = layersallproperties;
        let componentToHex = (c) => {
            var hex = c.toString(16);
            return hex.length == 1 ? "0" + hex : hex;
        }
        let rgb_to_hex = (color) => {                       
            let r = color[0] * 255;
            let g = color[1] * 255;
            let b = color[2] * 255;                     
            return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
        }
        for (let i = 0; i < layersallproperties.length; i++) {
            let props = layersallproperties[i];             
            this.layersfromGiD.push(props.name);         
            let color_format = 'hex';          
            let newlayer = new LayerRow_read({
                'treereference': this,
                'gidid': props.name,
                'name': this.child_name(props.name),
                'color_format': color_format,
                'color': rgb_to_hex(props.color),
                'layertouse': Boolean(props.to_use == "1"),
                'visible': Boolean(props.visible == "1"),
                'freeze': Boolean(props.frozen == "1"),
                'transparent': Boolean(props.opaque == "0"),
                'back': Boolean(props.back == "1")
            });
            if (props.to_use == "1") {
                this.current_layer_to_use = newlayer;
            }
            if (props.name.indexOf(expr) !== -1) {
                let p_name = this.parent_name(props.name);
                let curr_node = this.findChildrenRecursive(p_name, expr);
                if (curr_node) {
                    curr_node.addChildren(newlayer);
                } else {
                    console.log('trying to add a layer that parent do not exists')
                }
            } else {
                this.widget.addChildren(newlayer);
            }
        }
        this.widget.update(['data']);
    }
}
  //------------------------
  definition[id].laybut = new Button({
    'id':'laybut',
    'name': "laybut",    
    'icon': 'box',
    'icon_type': 'fas',   
    'translation': [ { id:"laybut", translations:{es:"Nuevas Capas",en:"New Layers"} }],
    'onClick': function () {        
        context.toggleWindow(definition[id].LayersTree_read, CurrentWorkingMode.mainwindow);
    },
  });
  definition['main_top_toolbar.js'].MainToolbar.addButton(definition[id].laybut);
  
  definition[id].LayersTree_read = new LayersTree_read({
    'name': "layers", 
    'id': "layers",
    'children': []
  });
  
  CurrentWorkingMode.addInSplitRight(definition[id].LayersTree_read);  
  // End of editable area
}
Dependencies = [ 'main_top_toolbar.js' ];