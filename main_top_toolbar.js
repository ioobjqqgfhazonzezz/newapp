listAllFunctionalities['main_top_toolbar.js'] = function (context, CurrentWorkingMode, definition, id = 'main_top_toolbar.js') { 
  // Start of editable area
  
  definition[id].MainToolbar = new Toolbar({
    'name': 'Main Toolbar',
    'children': []
  });
  CurrentWorkingMode.addTop(definition[id].MainToolbar);

  // End of editable area
}
