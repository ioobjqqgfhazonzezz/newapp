listAllFunctionalities['create_circle.js'] = function (context, CurrentWorkingMode, definition, id = 'create_circle.js') { 
  // Start of editable area
  // 

    // ------------- define a button and add to top --------------------
    definition[id].CircleButton = new Button({
        'id':'CircleButton',
        'tooltip': "btnAddCircle",
        'icon': 'circle',
        'icon_type': 'far',
        'translation': [ { id:"btnAddCircle", translations:{es:"Añadir círculo",en:"Add circle"} }],
        'onClick': function () {
            context.openWindow(definition[id].CreateCircleWindow, CurrentWorkingMode.mainwindow);
        },
    });
    definition['main_top_toolbar.js'].MainToolbar.addButton(definition[id].CircleButton);

    // ------------- define the window --------------------
    definition[id].CreateCircleWindow = new Window({
        'id': 'CreateCircleWindow',
        'name': "windCircle",
        'acceptLabel': "GiDSimulation005",
        'translation': [ { id:"windCircle", translations:{es:"Crear Círculo",en:"Create Circle"} }],
        'children': [
            definition[id].CircleCenterCoordinates = new Coordinates({
                'id':'CircleCenterCoordinates',
                'name': 'center',
                'value': ['0', '1', '2'],
            }),
            definition[id].CircleRadiusInput = new Input({
                'id':'CircleRadiusInput',
                'name': "GiDSimulation006",
                'tooltip': "GiDSimulation006",
                'value': '1',
            }),
        ],
       
        'onAccept': async function () {
            let center = await definition[id].CircleCenterCoordinates.value();
            context.createCircle(center, await definition[id].CircleRadiusInput.value());
        },
    });
    CurrentWorkingMode.addInSplitLeft(definition[id].CreateCircleWindow);

  // End of editable area
}
Dependencies = [ 'globalTranslations.js', 'main_top_toolbar.js' ];