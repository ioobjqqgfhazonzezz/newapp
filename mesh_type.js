listAllFunctionalities['mesh_type.js'] = function (context, CurrentWorkingMode, definition, id = 'mesh_type.js') { 
  // Start of editable area

  definition[id].MeshTypeDropDown = new DropDown({
    'id':"MeshTypeDropDown",
    'value': "none",
    'tooltip': "mesh_tooltip",
    'listOptions': [{label:"meshTypeNone", value:"none"},{label:"meshTypeTriangle",value:"triangle"}],
    // {label:"meshTypeQuadrilateral",value:"quadrilateral"},{label:"meshTypeCircle",value:"circle"}],
    'name': "GiDSimulation012",
    'tooltip': "GiDSimulation012",
    'translation': [{id:"mesh_tooltip", translations:{es:"Seleccionar el tipo de elemento",en:"Choose element type"}},{ id:"meshTypeNone", translations:{es:"ninguno", en:"none"} },{ id:"meshTypeTriangle", translations:{es:"triángulo", en:"triangle"} },{ id:"meshTypeQuadrilateral", translations:{es:"quadrilátero", en:"quadrilateral"} },{ id:"meshTypeCircle", translations:{es:"círculo", en:"circle"} }],   
  });

  definition[id].alltypesbut = new Button({
    'name': "alltypesbut",  
    'id': "alltypesbut",    
    'icon': 'pen',             
    'icon_type': 'fas',
    'tooltip': "alltypesbut_tooltip",
    'translation': [ { id:"alltypesbut", translations:{es:"Asignar a todo",en:"Assign all"} },{ id:"alltypesbut_tooltip", translations:{es:"Asignar a todas las superficies",en:"Assign to all surfaces"} }],
    'onClick': function () {
      if (definition[id].MeshTypeDropDown.value == "none") { return }
      context.AssignElementTypeAllSurfaces(definition[id].MeshTypeDropDown.value);
    },
  });

  definition[id].showelemtypesbut = new Button({
    'name': "showelemtypesbut",
    'id': "showelemtypesbut",      
    'icon': 'palette',
    'icon_type': 'fas',                   
    'tooltip': "show_assigned_el",      
    'translation': [ {id:"showelemtypesbut", translations:{es:"Tipos de elementos",en:"Element types"}},{id:"show_assigned_el", translations:{es:"Mostrar tipos de elementos asignados", en:"Show/hide assigned element types"}}, { id:"alltypesbut", translations:{es:"Mostrar/ocultar los tipos de elementos asignados",en:"Show/hide assigned element types"} }],
    'onClick': function () {     
      context.toggleElementTypeVisualization();
    },
  });

  definition['main_top_toolbar.js'].MainToolbar.addButton(definition[id].MeshTypeDropDown);
  definition['main_top_toolbar.js'].MainToolbar.addButton(definition[id].alltypesbut); 
  definition['main_top_toolbar.js'].MainToolbar.addButton(definition[id].showelemtypesbut); 

  // End of editable area
}
Dependencies = [ 'globalTranslations.js', 'main_top_toolbar.js' ];