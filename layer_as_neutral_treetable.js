listAllFunctionalities['layer_as_neutral_treetable.js'] = function (context, CurrentWorkingMode, definition, id = 'layer_as_neutral_treetable.js') { 
  // Start of editable area
  definition[id].laybut = new Button({
    'id':'laybut',
    'name': "laybut",    
    'icon': 'box',
    'icon_type': 'fas',   
    'translation': [ { id:"laybut", translations:{es:"Nuevas Capas",en:"New Layers"} }],
    'onClick': function () {        
        context.toggleWindow(definition[id].LayersTree, CurrentWorkingMode.mainwindow);
    },
  });
  definition['main_top_toolbar.js'].MainToolbar.addButton(definition[id].laybut);
  
  definition[id].LayersTree = new LayersTree({
    'name': "layers", 
    'id': "layers",
    'children': []
  });
  
  CurrentWorkingMode.addInSplitRight(definition[id].LayersTree);  
  // End of editable area
}
Dependencies = [ 'main_top_toolbar.js' ];